//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.propertysheet;

import net.ubos.underbars.resources.ResourceManager;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.AbstractHtmlHandlebarsViewletFactoryChoice;
import net.ubos.underbars.vl.HandlebarsViewlet;
import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Overrides the recommended Viewlet when generating the POST URL. If we don't do this,
 * editing, say, Attributes, will, upon submit, attempt to run the AttributeTableVL,
 * but that works for in inlined placement only.
 */
public class PropertySheetViewletFactoryChoice
        extends
            AbstractHtmlHandlebarsViewletFactoryChoice
{
    private static final Log log = Log.getLogInstance( PropertySheetViewletFactoryChoice.class ); // our own, private logger

    /**
     * Factory method
     *
     * @param toView the WebMeshObjectsToView to view
     * @param viewletName name of the Viewlet for which this is a choice
     * @param viewletTypes set of Viewlet types supported by the Viewlet
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param matchQuality the quality of the match
     * @return return the choice
     */
    public static PropertySheetViewletFactoryChoice [] createSeveral(
            String                viewletName,
            ResourceManager       resourceManager,
            ResourceHelper        resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1 )
    {
        TemplateMap templateMap = resourceManager.getViewletTemplateMapFor( viewletName );

        if( templateMap != null ) {
            return new PropertySheetViewletFactoryChoice[] {
                new PropertySheetViewletFactoryChoice(
                        viewletName,
                        templateMap,
                        placement1,
                        dimensionality1,
                        detail1,
                        matchQuality1,
                        resourceHelper )
            };

        } else {
            log.error( "Null TemplateMap for viewlet " + viewletName );
            return null;
        }
    }

    /**
     * Factory method
     *
     * @param toView the WebMeshObjectsToView to view
     * @param viewletName name of the Viewlet for which this is a choice
     * @param viewletTypes set of Viewlet types supported by the Viewlet
     * @param resourceManager where to get resources from
     * @param resourceHelper the ResourceHelper to use
     * @param matchQuality the quality of the match
     * @return return the choice
     */
    public static PropertySheetViewletFactoryChoice [] createSeveral(
            String               viewletName,
            ResourceManager      resourceManager,
            ResourceHelper       resourceHelper,
            WebViewletPlacement   placement1,
            ViewletDimensionality dimensionality1,
            ViewletDetail         detail1,
            double                matchQuality1,
            WebViewletPlacement   placement2,
            ViewletDimensionality dimensionality2,
            ViewletDetail         detail2,
            double                matchQuality2 )
    {
        TemplateMap templateMap = resourceManager.getViewletTemplateMapFor( viewletName );

        if( templateMap != null ) {
            return new PropertySheetViewletFactoryChoice[] {
                new PropertySheetViewletFactoryChoice(
                        viewletName,
                        templateMap,
                        placement1,
                        dimensionality1,
                        detail1,
                        matchQuality1,
                        resourceHelper ),
                new PropertySheetViewletFactoryChoice(
                        viewletName,
                        templateMap,
                        placement2,
                        dimensionality2,
                        detail2,
                        matchQuality2,
                        resourceHelper )
            };

        } else {
            log.error( "Null TemplateMap for viewlet " + viewletName );
            return null;
        }
    }

    /**
     * Constructor.
     *
     * @param toView the WebMeshObjectsToView to view
     * @param viewletName name of the Viewlet for which this is a choice
     * @param viewletTypes set of Viewlet types supported by the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param resourceHelper the ResourceHelper to use
     * @param matchQuality the quality of the match
     */
    protected PropertySheetViewletFactoryChoice(
            String                viewletName,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail,
            double                matchQuality,
            ResourceHelper        resourceHelper )
    {
        super( viewletName, templateMap, placement, dimensionality, detail, matchQuality, resourceHelper );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HandlebarsViewlet instantiateViewlet()
        throws
            CannotViewException
    {
        return PropertySheetViewlet.create( theName, theTemplateMap, thePlacement, theDimensionality, theDetail );
    }
}
