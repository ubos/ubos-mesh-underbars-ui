//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.propertysheet;

import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.AbstractHtmlHandlebarsViewlet;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.DefaultWebViewedMeshObjects;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;
import net.ubos.web.vl.WebViewletState;

/**
 *
 */
public class PropertySheetViewlet
    extends
        AbstractHtmlHandlebarsViewlet
{
    /**
     * Factory method.
     */
    public static PropertySheetViewlet create(
            String                name,
            TemplateMap           templateMap,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail )
    {
        DefaultWebViewedMeshObjects viewed = new DefaultWebViewedMeshObjects( placement, dimensionality, detail );

        PropertySheetViewlet ret = new PropertySheetViewlet( name, templateMap, viewed );

        viewed.setViewlet( ret );
        return ret;
    }

    /**
     * Constructor.
     */
    protected PropertySheetViewlet(
            String               name,
            TemplateMap          templateMap,
            WebViewedMeshObjects viewed )
    {
        super( name, templateMap, viewed );
    }

    /**
    * {@inheritDoc}
    */
   @Override
   public String getPostUrl(
           WebMeshObjectsToView toView )
   {
       toView = toView.withViewletState( WebViewletState.VIEW );
       toView = toView.withRequiredViewletName( null );
       toView = toView.withRecommendedViewletName( "net.ubos.underbars.vl.propertysheet" );
       return toView.asUrlString();
   }
}
