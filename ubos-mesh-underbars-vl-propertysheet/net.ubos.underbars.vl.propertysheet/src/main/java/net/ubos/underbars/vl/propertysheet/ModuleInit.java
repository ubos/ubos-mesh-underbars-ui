//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.propertysheet;

import java.io.IOException;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Module initializer class.
     */
public class ModuleInit
{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-vl-propertysheet";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );
            String mainVlName = "net.ubos.underbars.vl.propertysheet";

            Underbars.registerViewlet(
                    (MeshObjectsToView toView) -> {
                        if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                            return null;
                        }

                        return PropertySheetViewletFactoryChoice.createSeveral(
                                mainVlName,
                                resourceManager,
                                ResourceHelper.getInstance( mainVlName + ".VL", ModuleInit.class.getClassLoader() ),
                                WebViewletPlacement.ROOT,    ViewletDimensionality.DIM_2D, ViewletDetail.NORMAL,   ViewletFactoryChoice.FALLBACK_MATCH_QUALITY );
                    } );

            String overviewVlName = "net.ubos.underbars.vl.genericsummary";

            Underbars.registerViewlet(
                    (MeshObjectsToView toView) -> {
                        if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                            return null;
                        }

                        return PropertySheetViewletFactoryChoice.createSeveral(
                                overviewVlName,
                                resourceManager,
                                ResourceHelper.getInstance( overviewVlName + ".VL", ModuleInit.class.getClassLoader() ),
                                WebViewletPlacement.INLINED,    ViewletDimensionality.DIM_2D, ViewletDetail.OVERVIEW, ViewletFactoryChoice.FALLBACK_MATCH_QUALITY );
                    } );

            String [] otherVlNames = {
                "net.ubos.underbars.vl.attributetable",
                "net.ubos.underbars.vl.propertytable",
                "net.ubos.underbars.vl.roleattributetable",
                "net.ubos.underbars.vl.rolepropertytable"
            };

            for( String vlName : otherVlNames ) {
                Underbars.registerViewlet(
                        (MeshObjectsToView toView) -> {
                            if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                                return null;
                            }

                            return PropertySheetViewletFactoryChoice.createSeveral(
                                    vlName,
                                    resourceManager,
                                    ResourceHelper.getInstance( vlName + ".VL", ModuleInit.class.getClassLoader() ),
                                    WebViewletPlacement.INLINED, ViewletDimensionality.DIM_2D, ViewletDetail.NORMAL, ViewletFactoryChoice.WORST_MATCH_QUALITY );
                                    // exists but pre-empted by everything else including the
                                    // genericsummary above
                            });
            }


            Underbars.registerAssets( resourceManager.getAssetMap() );

            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }
}
