//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.meshbase;

import java.util.function.Predicate;
import java.util.regex.Pattern;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.AbstractMeshObjectPaginatingHtmlHandlebarsViewlet;
import net.ubos.util.ArrayHelper;
import net.ubos.util.ResourceHelper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.FilteringCursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.DefaultWebViewedMeshObjects;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Displays the content of a MeshBase
 */
public class MeshBaseViewlet
    extends
        AbstractMeshObjectPaginatingHtmlHandlebarsViewlet
{
    private static final Log log = Log.getLogInstance( MeshBaseViewlet.class );

    /**
     * Factory method.
     *
     * @param name the Viewlet name
     * @param templateMap the named Templates used for the Viewlet
     * @return the created Viewlet
     */
    public static MeshBaseViewlet create(
            String                             name,
            TemplateMap                        templateMap,
            WebViewletPlacement                placement,
            ViewletDimensionality              dimensionality,
            ViewletDetail                      detail,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        return create( name, templateMap, DEFAULT_PAGE_LENGTH, placement, dimensionality, detail, idSerializer );
    }

    /**
     * Factory method.
     *
     * @param name the Viewlet name
     * @param templateMap the named Templates used for the Viewlet
     * @param tagMap defines the tags used in the template
     * @param defaultPageLength the default page length
     * @return the created Viewlet
     */
    public static MeshBaseViewlet create(
            String                             name,
            TemplateMap                        templateMap,
            int                                defaultPageLength,
            WebViewletPlacement                placement,
            ViewletDimensionality              dimensionality,
            ViewletDetail                      detail,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        DefaultWebViewedMeshObjects viewed = new DefaultWebViewedMeshObjects( placement, dimensionality, detail );

        MeshBaseViewlet ret = new MeshBaseViewlet(
                name,
                templateMap,
                viewed,
                defaultPageLength,
                idSerializer );

        viewed.setViewlet( ret );
        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param viewletName the computable name of the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param tagMap defines the tags used in the template
     * @param viewed the WebViewedMeshObjects to use
     * @param defaultPageLength the default page length
     */
    protected MeshBaseViewlet(
            String                             viewletName,
            TemplateMap                        templateMap,
            WebViewedMeshObjects               viewed,
            int                                defaultPageLength,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        super( viewletName, templateMap, viewed, defaultPageLength, idSerializer );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CursorIterator<MeshObject> getContentIterator()
    {
        if( theContentIterator == null ) {
            WebViewedMeshObjects viewed = getViewedMeshObjects();
            MeshBase             mb     = getSubject().getMeshBase();

            String    regexString       = (String) viewed.getViewletParameter( IDENTIFIER_REGEX_PARNAME );
            Object [] entityTypesString = viewed.getMultivaluedViewletParameter( ENTITY_TYPES_PARNAME );

            Pattern       regexTmp       = null; // Java can be annoying
            EntityType [] entityTypesTmp = null;

            if( regexString != null && regexString.length() > 0 ) {
                regexTmp = Pattern.compile( regexString );
            }
            if( entityTypesString != null && entityTypesString.length > 0 ) {
                int count = 0;
                entityTypesTmp = new EntityType[ entityTypesString.length ];
                for( int i=0 ; i<entityTypesString.length ; ++i ) {
                    try {
                        EntityType found = ModelBase.SINGLETON.findEntityTypeOrNull( (String) entityTypesString[i] );
                        if( found != null ) {
                            entityTypesTmp[ count++ ] = found;
                        }
                    } catch( IllegalArgumentException ex ) {
                        log.debug( ex );
                    }
                }
                if( count == 0 ) {
                    entityTypesTmp = null;
                } else if( count < entityTypesString.length ) {
                    entityTypesTmp = ArrayHelper.copyIntoNewArray( entityTypesTmp, 0, count, EntityType.class );
                }
            }

            Pattern       regex       = regexTmp; // Java can be annoying
            EntityType [] entityTypes = entityTypesTmp;

            CursorIterator<MeshObject> ret = mb.iterator();
            if( regex != null || entityTypes != null ) {

                Predicate<MeshObject> filter = ( MeshObject o ) -> {
                    if( regex != null && !regex.matcher( theIdSerializer.toExternalForm( o.getIdentifier())).matches() ) {
                        return false;
                    }
                    if( entityTypes != null ) {
                        boolean found = false;
                        for( EntityType current : entityTypes ) {
                            if( o.isBlessedBy( current )) {
                                found = true;
                                break;
                            }
                        }
                        if( !found ) {
                            return false;
                        }
                    }
                    return true;
                };

                ret = FilteringCursorIterator.create( ret, filter );
            }
            theContentIterator = ret;
        }
        return theContentIterator;
    }

    /**
     * The iterator over the content, allocated as needed.
     */
    protected CursorIterator<MeshObject> theContentIterator;

    /**
     * Name of the Viewlet Parameter that holds the identifier regex for filtering.
     */
    public static final String IDENTIFIER_REGEX_PARNAME = "identifier-regex";

    /**
     * Name of the Viewlet Parameter that holds the EntityTypes by which we filter.
     */
    public static final String ENTITY_TYPES_PARNAME = "show-types";

    /**
     * Default page size.
     */
    public static final int DEFAULT_PAGE_LENGTH = ResourceHelper.getInstance( MeshBaseViewlet.class ).getResourceIntegerOrDefault(
            "DefaultPageLength",
            20 );
}
