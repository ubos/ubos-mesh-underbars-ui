//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.advertisementinteraction;

import java.io.IOException;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.underbars.vl.DefaultHtmlHandlebarsViewletFactoryChoice;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Module initializer class.
 */
public class ModuleInit
{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-vl-advertisementinteraction";

    /**
     * Name of the Viewlet.
     */
    public static final String VL_NAME = "net.ubos.underbars.vl.advertisementinteraction";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if Module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

            Underbars.registerViewlet(
                    (MeshObjectsToView toView) -> {
                        if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                            return null;
                        }

                        if( toView.getSubject().isBlessedBy( MarketingSubjectArea.ADVERTISEMENTINTERACTION )) {
                            return DefaultHtmlHandlebarsViewletFactoryChoice.createSeveral(
                                    VL_NAME,
                                    resourceManager,
                                    ResourceHelper.getInstance( VL_NAME + ".VL", ModuleInit.class.getClassLoader() ),
                                    WebViewletPlacement.ROOT,    ViewletDimensionality.DIM_2D, ViewletDetail.NORMAL,   ViewletFactoryChoice.GOOD_MATCH_QUALITY,
                                    WebViewletPlacement.INLINED, ViewletDimensionality.DIM_2D, ViewletDetail.OVERVIEW, ViewletFactoryChoice.AVERAGE_MATCH_QUALITY );
                        } else {
                            return null;
                        }
                    } );


            Underbars.registerAssets( resourceManager.getAssetMap() );

            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }
}
