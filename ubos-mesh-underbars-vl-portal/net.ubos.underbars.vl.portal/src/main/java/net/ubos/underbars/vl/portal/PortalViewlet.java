//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.portal;

import java.text.ParseException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.set.ByTypeMeshObjectSelector;
import net.ubos.mesh.set.DefaultMeshObjectSorter;
import net.ubos.mesh.set.MeshObjectSet;
import net.ubos.mesh.set.MeshObjectSorter;
import net.ubos.mesh.set.OrderedMeshObjectSet;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.index.MeshBaseIndex;
import net.ubos.model.primitives.EntityType;
import net.ubos.model.primitives.MeshTypeIdentifier;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.modelbase.MeshTypeNotFoundException;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.AbstractMeshObjectPaginatingHtmlHandlebarsViewlet;
import net.ubos.util.ResourceHelper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.ZeroElementCursorIterator;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.DefaultWebViewedMeshObjects;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Displays the HomePort Portal
 */
public class PortalViewlet
    extends
        AbstractMeshObjectPaginatingHtmlHandlebarsViewlet
{
    /**
     * Factory method.
     *
     * @param templateMap the named Templates used for the Viewlet
     * @param idSerializer knows how to serialize MeshObjectIdentifiers for pagination purposes
     * @return the created Viewlet
     */
    public static PortalViewlet create(
            TemplateMap                        templateMap,
            WebViewletPlacement                placement,
            ViewletDimensionality              dimensionality,
            ViewletDetail                      detail,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        return create( templateMap, DEFAULT_PAGE_LENGTH, placement, dimensionality, detail, idSerializer );
    }

    /**
     * Factory method.
     *
     * @param templateMap the named Templates used for the Viewlet
     * @param defaultPageLength the default page length
     * @param idSerializer knows how to serialize MeshObjectIdentifiers for pagination purposes
     * @return the created Viewlet
     */
    public static PortalViewlet create(
            TemplateMap                        templateMap,
            int                                defaultPageLength,
            WebViewletPlacement                placement,
            ViewletDimensionality              dimensionality,
            ViewletDetail                      detail,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        DefaultWebViewedMeshObjects viewed = new DefaultWebViewedMeshObjects( placement, dimensionality, detail );

        PortalViewlet ret = new PortalViewlet(
                "net.ubos.underbars.vl.portal",
                templateMap,
                viewed,
                defaultPageLength,
                idSerializer );

        viewed.setViewlet( ret );
        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param viewletName the computable name of the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param viewed the WebViewedMeshObjects to use
     * @param defaultPageLength the default page length
     */
    protected PortalViewlet(
            String                             viewletName,
            TemplateMap                        templateMap,
            WebViewedMeshObjects               viewed,
            int                                defaultPageLength,
            MeshObjectIdentifierBothSerializer idSerializer )
    {
        super( viewletName, templateMap, viewed, defaultPageLength, idSerializer );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CursorIterator<MeshObject> getContentIterator()
    {
        if( theContentIterator == null ) {
            WebViewedMeshObjects viewed = getViewedMeshObjects();

            String entityTypeString = (String) viewed.getViewletParameter( ENTITY_TYPES_PARNAME );
            String searchString     = (String) viewed.getViewletParameter( SEARCH_PARNAME );

            searchString = searchString == null ? null : searchString.trim();

            CursorIterator<MeshObject> ret;

            MeshBase      mb    = getSubject().getMeshBase();
            MeshBaseIndex index = mb.getMeshBaseIndex();

            try {
                if( index == null ) {
                    ret = ZeroElementCursorIterator.create();

                } else if( searchString != null && !searchString.isEmpty() ) {

                    EntityType entityType;
                    if( entityTypeString != null && !entityTypeString.isEmpty() ) {
                        entityType = ModelBase.SINGLETON.findEntityType( entityTypeString );
                    } else {
                        entityType = null;
                    }

                    MeshObjectSet found = index.searchFullTextWithCount( searchString );
                    if( entityType != null ) {
                        found = found.subset( ByTypeMeshObjectSelector.create( entityType ));
                    }
                    ret = ordered( found ).iterator();

                } else if( entityTypeString != null && !entityTypeString.isEmpty() ) {

                    EntityType entityType;
                    if( entityTypeString != null && !entityTypeString.isEmpty() ) {
                        entityType = ModelBase.SINGLETON.findEntityType( entityTypeString );
                    } else {
                        entityType = null;
                    }
                    if( entityType != null ) {
                        MeshObjectSet found = index.searchForBlessedWithEntityType( entityType );
                        ret = ordered( found ).iterator();
                    } else {
                        ret = ZeroElementCursorIterator.create();
                    }

                } else {
                    // don't show anything
                    ret = ZeroElementCursorIterator.create();
                }

            } catch( ParseException ex ) {
                ret = ZeroElementCursorIterator.create();

            } catch( MeshTypeNotFoundException ex ) {
                ret = ZeroElementCursorIterator.create();
            }

            theContentIterator = ret;
        }
        return theContentIterator;
    }

    /**
     * Make it easy for the Handlebars template to determine whether a search is active.
     *
     * @return the search terms, or null
     */
    public String getSearchTerms()
    {
        WebViewedMeshObjects viewed = getViewedMeshObjects();

        String searchString = (String) viewed.getViewletParameter( SEARCH_PARNAME );

        return searchString;
    }

    /**
     * Make it easy for the Handlebars template to determine whether an EntityType has been selected.
     *
     * @return the name of the EntityType, or null
     */
    public String getShowTypesUserVisibleName()
    {
        WebViewedMeshObjects viewed = getViewedMeshObjects();

        String entityTypeString = (String) viewed.getViewletParameter( ENTITY_TYPES_PARNAME );

        if( entityTypeString == null || entityTypeString.isEmpty()) {
            return null;
        } else {
            EntityType entityType = ModelBase.SINGLETON.findEntityTypeOrNull( entityTypeString );
            return entityType.getUserVisibleName().value();
        }
    }

    /**
     * Make it easy for the Handlebars template to determine whether any filter is in effect.
     *
     * @return true if a filter is in effect
     */
    public boolean getIsFiltered()
    {
        WebViewedMeshObjects viewed = getViewedMeshObjects();

        String entityTypeString = (String) viewed.getViewletParameter( ENTITY_TYPES_PARNAME );
        String searchString     = (String) viewed.getViewletParameter( SEARCH_PARNAME );

        return    ( entityTypeString != null && !entityTypeString.isEmpty())
               || ( searchString     != null && !searchString.isEmpty());
    }

    /**
     * Make it easy for the Handlebars template to determine whether an EntityType has been selected.
     *
     * @return the name of the EntityType, or null
     */
    public MeshTypeIdentifier getShowTypesIdentifier()
    {
        WebViewedMeshObjects viewed = getViewedMeshObjects();

        String entityTypeString = (String) viewed.getViewletParameter( ENTITY_TYPES_PARNAME );

        if( entityTypeString == null || entityTypeString.isEmpty()) {
            return null;
        } else {
            EntityType entityType = ModelBase.SINGLETON.findEntityTypeOrNull( entityTypeString );
            return entityType.getIdentifier();
        }
    }

    /**
     * Makes things easier in the template.
     *
     * @return true if the MeshBase only has the home object
     */
    public boolean getEmptyMeshBase()
    {
        return theViewedMeshObjects.getSubject().getMeshBase().size() <= 1 ;
    }

    /**
     * Helper method to sort a MeshObjectSet correctly.
     *
     * FIXME: this needs to take Viewlet Parameters
     *
     * @param input the ordered set
     * @return the ordered set
     */
    protected OrderedMeshObjectSet ordered(
            MeshObjectSet input )
    {
        MeshObjectSorter mos = DefaultMeshObjectSorter.BY_TIME_CREATED;

        OrderedMeshObjectSet ret = input.ordered( mos );
        return ret;
    }

    /**
     * The iterator over the content, allocated as needed.
     */
    protected CursorIterator<MeshObject> theContentIterator;

    /**
     * Name of the Viewlet Parameter that holds the EntityTypes by which we filter.
     */
    public static final String ENTITY_TYPES_PARNAME = "show-types";

    /**
     * Name of the Viewlet Parameter that holds the search text by which we filter
     */
    public static final String SEARCH_PARNAME = "search";

    /**
     * Default page size.
     */
    public static final int DEFAULT_PAGE_LENGTH = ResourceHelper.getInstance( PortalViewlet.class ).getResourceIntegerOrDefault(
            "DefaultPageLength",
            20 );
}
