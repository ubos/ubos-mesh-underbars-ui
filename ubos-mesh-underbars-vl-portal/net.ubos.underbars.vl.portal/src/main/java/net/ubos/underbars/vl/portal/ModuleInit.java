//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.portal;

import java.io.IOException;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.ExternalNameHashMeshObjectIdentifierBothSerializer;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.externalized.MeshObjectIdentifierBothSerializer;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.HandlebarsViewlet;
import net.ubos.util.ResourceHelper;
import net.ubos.util.logging.Log;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.AbstractWebViewletFactoryChoice;
import net.ubos.web.vl.WebViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Module initializer class.
     */
public class ModuleInit
{
    private static final Log log = Log.getLogInstance( ModuleInit.class ); // our own, private logger

    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-vl-portal";

    /**
     * Name of the Viewlet.
     */
    public static final String VL_NAME = "net.ubos.underbars.vl.portal";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

            TemplateMap templateMap = resourceManager.getViewletTemplateMapFor( VL_NAME );

            if( templateMap != null ) {
                Underbars.registerViewlet(
                        ( MeshObjectsToView toView ) -> {
                        if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                            return null;
                        }

                            if( toView.getSubject().isHomeObject() ) {
                                return new WebViewletFactoryChoice [] {
                                        new MyViewletFactoryChoice( templateMap )
                                };
                            }
                            return null;
                        } );

            } else {
                log.error( "Cannot find ViewletTemplateMap for", VL_NAME );
            }

            Underbars.registerAssets( resourceManager.getAssetMap() );

            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }

    public static class MyViewletFactoryChoice
        extends
            AbstractWebViewletFactoryChoice
    {
        public MyViewletFactoryChoice(
                TemplateMap templateMap )
        {
            super( VL_NAME,
                   SelectableMimeType.TEXT_HTML.getMimeType(),
                   WebViewletPlacement.ROOT,
                   ViewletDimensionality.DIM_2D,
                   ViewletDetail.NORMAL,
                   ViewletFactoryChoice.PERFECT_MATCH_QUALITY,
                   ResourceHelper.getInstance( VL_NAME + ".VL", ModuleInit.class.getClassLoader() ));

            theTemplateMap = templateMap;
        }

        @Override
        public HandlebarsViewlet instantiateViewlet()
        {
            MeshBase mb = Underbars.getMeshBaseNameServer().getDefaultMeshBase();

            MeshObjectIdentifierBothSerializer idSerializer = ExternalNameHashMeshObjectIdentifierBothSerializer.create(
                    mb.getPrimaryNamespaceMap(), mb );

            return PortalViewlet.create( theTemplateMap, thePlacement, theDimensionality, theDetail, idSerializer );
        }

        protected final TemplateMap theTemplateMap;
    }
}
