//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.mediaproperty;

import java.io.IOException;
import java.text.ParseException;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.security.ThreadIdentityManager;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.PropertyType;
import net.ubos.modelbase.ModelBase;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Displays media such as images.
 */
public class ModuleInit
{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-vl-mediaproperty";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

            Underbars.registerViewlet(
                    (MeshObjectsToView toView) -> {
                        if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                            return null;
                        }

                        MeshObject   subject      = toView.getSubject();
                        Object       propertyType = toView.getViewletParameter( MediapropertyViewlet.PROPERTY_TYPE_VIEWLET_PAR );

                        PropertyType toShow = null;
                        BlobValue    value  = null;

                        if( propertyType instanceof PropertyType ) {
                            toShow = (PropertyType) propertyType;
                        } else if( propertyType instanceof String ) {
                            try {
                                toShow = ModelBase.SINGLETON.findPropertyTypeOrNull(
                                        MediapropertyViewlet.TYPE_SERIALIZER.fromExternalForm( (String) propertyType ));

                            } catch( ParseException ex ) {
                                // ignore
                            }
                        }

                        if( toShow == null ) {
                            // let's guess
                            PropertyType [] allTypes = subject.getPropertyTypes();

                            for( PropertyType current : allTypes ) {
                                if( !( current.getDataType() instanceof BlobDataType )) {
                                    continue;
                                }
                                value = (BlobValue) subject.getPropertyValue( current );
                                if( value == null ) {
                                    continue;
                                }
                                if( MediapropertyViewlet.isSupportedImageMime( value.getMimeType() )) {
                                    toShow = current;
                                    break;
                                }
                                if( MediapropertyViewlet.isSupportedVideoMime( value.getMimeType() )) {
                                    toShow = current;
                                    break;
                                }
                            }
                        } else {
                            value = (BlobValue) subject.getPropertyValue( toShow );
                            if( value == null ) {
                                toShow = null; // can't do it
                            }
                        }

                        if( toShow == null ) {
                            return null;
                        } else {
                            double matchQuality = ViewletFactoryChoice.FALLBACK_MATCH_QUALITY;
                                    // we rather have EntityType-specific Viewlets that can also
                                    // show other Properties

                            return new MediapropertyViewlet.FactoryChoice[] {
                                new MediapropertyViewlet.FactoryChoice(
                                        toShow,
                                        value.getMimeType(),
                                        WebViewletPlacement.ROOT,
                                        ViewletDimensionality.DIM_2D,
                                        ViewletDetail.NORMAL,
                                        matchQuality ),
                                new MediapropertyViewlet.FactoryChoice(
                                        toShow,
                                        value.getMimeType(),
                                        WebViewletPlacement.INLINED,
                                        ViewletDimensionality.DIM_2D,
                                        ViewletDetail.NORMAL,
                                        matchQuality ),
                            };
                        }
                    } );

            Underbars.registerAssets( resourceManager.getAssetMap() );
            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }
}
