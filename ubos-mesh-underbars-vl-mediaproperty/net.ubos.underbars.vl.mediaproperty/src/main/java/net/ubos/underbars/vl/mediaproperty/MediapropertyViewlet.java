//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.mediaproperty;

import java.io.IOException;
import java.util.Set;
import net.ubos.mesh.MeshObject;
import net.ubos.model.primitives.BlobValue;
import net.ubos.model.primitives.MeshTypeIdentifierBothSerializer;
import net.ubos.model.primitives.PropertyType;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.modelbase.m.DefaultMMeshTypeIdentifierBothSerializer;
import net.ubos.util.ResourceHelper;
import net.ubos.util.StringHelper;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.AbstractWebViewlet;
import net.ubos.web.vl.AbstractWebViewletFactoryChoice;
import net.ubos.web.vl.DefaultWebViewedMeshObjects;
import net.ubos.web.vl.StructuredResponse;
import net.ubos.web.vl.ToViewStructuredResponsePair;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * A Viewlet that can show media such as images.
 */
public class MediapropertyViewlet
    extends
        AbstractWebViewlet
{
    /**
     * The Viewlet's name.
     */
    public static final String VL_NAME = "net.ubos.underbars.vl.mediaproperty";

    /**
     * Factory method.
     */
    public static MediapropertyViewlet create(
            PropertyType          toShow,
            WebViewletPlacement   placement,
            ViewletDimensionality dimensionality,
            ViewletDetail         detail )
    {
        DefaultWebViewedMeshObjects  viewed = new DefaultWebViewedMeshObjects( placement, dimensionality, detail );

        MediapropertyViewlet ret = new MediapropertyViewlet( toShow, viewed );

        viewed.setViewlet( ret );
        return ret;
    }

    /**
     * Constructor.
     *
     * @param toShow the PropertyType whose value shall be shown
     */
    protected MediapropertyViewlet(
            PropertyType         toShow,
            WebViewedMeshObjects viewed )
    {
        super( VL_NAME, viewed );

        thePropertyType = toShow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processRequest(
            ToViewStructuredResponsePair rr )
        throws
            IOException
    {
        MeshObject subject = getSubject();
        BlobValue  value   = (BlobValue) subject.getPropertyValue( thePropertyType );
        String     mime    = value.getMimeType();

        WebViewedMeshObjects viewed   = (WebViewedMeshObjects) theViewedMeshObjects;
        StructuredResponse   response = rr.getStructuredResponse();

        boolean inlined = theViewedMeshObjects.getViewletPlacement() == WebViewletPlacement.INLINED;
        if( inlined ) {
            WebMeshObjectsToView toView = viewed.getMeshObjectsToView();
            toView = toView.withRequiredViewletPlacement( WebViewletPlacement.ROOT );
            toView = toView.withRequiredViewletName( VL_NAME ); // use self
            toView = toView.withViewletParameter( PROPERTY_TYPE_VIEWLET_PAR, TYPE_SERIALIZER.toExternalForm( thePropertyType.getIdentifier()));

            StringBuilder buf = new StringBuilder();
            buf.append( "<div class=\"" );
            buf.append( getCssClass() );
            buf.append( " vl\">\n" );

            if( isSupportedImageMime( mime )) {
                buf.append( " <img src=\"" );
                buf.append( StringHelper.stringToHtml( toView.asUrlString() ));
                buf.append( "\">\n" );

            } else if( isSupportedVideoMime( mime )) {
                buf.append( " <video controls>\n" );
                buf.append( "  <source src=\"" );
                buf.append( StringHelper.stringToHtml( toView.asUrlString() ));
                buf.append( "\" type=\"" );
                buf.append( mime );
                buf.append( "\">\n" );
                buf.append( "   Your browser does not support the video tag.\n" );
                buf.append( " </video>\n" );

            } else {
                buf.append( " Unexpected mime type in " ).append( getClass().getName() ).append( ": " ).append( mime );
            }
            buf.append( "</div>\n" );

            response.setSectionContent( StructuredResponse.HTML_BODY_INLINED_SECTION, buf.toString() );

            response.ensureHtmlHeaderLink( "stylesheet", "/s/net.ubos.underbars.vl.mediaproperty/net.ubos.underbars.vl.mediaproperty.css" );

        } else {
            response.setBinaryContent( value.getValue(), mime );
        }

    }

    /**
     * Is this an image mime type that this Viewlet can show
     *
     * @param mime the candidate mime type
     * @return true or false
     */
    public static boolean isSupportedImageMime(
            String mime )
    {
        return theSupportedImageTypes.contains( mime );
    }

    /**
     * Is this a video mime type that this Viewlet can show
     *
     * @param mime the candidate mime type
     * @return true or false
     */
    public static boolean isSupportedVideoMime(
            String mime )
    {
        return theSupportedVideoTypes.contains( mime );
    }

    /**
     * The PropertyType to show.
     */
    protected final PropertyType thePropertyType;

    /**
     * The set of supported image content types.
     */
    protected static final Set<String> theSupportedImageTypes = Set.of(
            SelectableMimeType.IMAGE_GIF.getMimeType(),
            SelectableMimeType.IMAGE_JPEG.getMimeType(),
            SelectableMimeType.IMAGE_PNG.getMimeType() );

    /**
     * The set of supported video content types.
     */
    protected static final Set<String> theSupportedVideoTypes = Set.of(
            SelectableMimeType.VIDEO_MP4.getMimeType());

    /**
     * Name of a Viewlet parameter that indicates the PropertyType whose content
     * shall be shown.
     */
    public static final String PROPERTY_TYPE_VIEWLET_PAR = "propertyType";

    /**
     * How we serialize the PropertyTypeIdentifier.
     */
    protected static final MeshTypeIdentifierBothSerializer TYPE_SERIALIZER = DefaultMMeshTypeIdentifierBothSerializer.SINGLETON;

    /**
     * Our ResourceHelper.
     */
    private static final ResourceHelper theResourceHelper = ResourceHelper.getInstance(VL_NAME + ".VL", ModuleInit.class.getClassLoader() );

    /**
     * The ViewletFactoryChoice for the MediapropertyViewlet.
     */
    public static class FactoryChoice
        extends
            AbstractWebViewletFactoryChoice
    {
        /**
         * Constructor.
         *
         * @param toView the MeshObjectsToView for which this is a choice
         * @param toShow the PropertyType whose value we show
         * @param matchQuality the quality of the match
         */
        public FactoryChoice(
                PropertyType          toShow,
                String                contentType,
                WebViewletPlacement   placement,
                ViewletDimensionality dimensionality,
                ViewletDetail         detail,
                double                matchQuality )
        {
            super( VL_NAME, contentType, placement, dimensionality, detail, matchQuality, theResourceHelper );

            thePropertyType = toShow;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MediapropertyViewlet instantiateViewlet()
        {
            return create( thePropertyType, thePlacement, theDimensionality, theDetail );
        }

        /**
         * The PropertyType to show.
         */
        protected final PropertyType thePropertyType;
    }
}
