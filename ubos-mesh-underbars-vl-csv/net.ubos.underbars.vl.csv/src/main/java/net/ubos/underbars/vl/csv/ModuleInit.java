//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.csv;

import java.io.IOException;
import net.ubos.daemon.Daemon;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.nonblessed.NonblessedCsvUtils;
import net.ubos.mesh.nonblessed.NonblessedUtils;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.underbars.vl.DefaultHtmlHandlebarsViewletFactoryChoice;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Module initializer class.
 */
public class ModuleInit
{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-vl-csv";

    /**
     * Name of the Viewlet.
     */
    public static final String VL_NAME = "net.ubos.underbars.vl.csv";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        try {
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

            Underbars.registerViewlet(
                    (MeshObjectsToView toView) -> {
                        if( !Daemon.getLicenseManager().isPermittedToUsePackage( UBOS_PACKAGE_NAME )) {
                            return null;
                        }

                        MeshObject subject = toView.getSubject();

                        if(    subject.hasAttribute( NonblessedUtils.OBJECTTYPE_ATTRIBUTE )
                            && NonblessedCsvUtils.OBJECTTYPE_VALUE_CSVFILE.equals( subject.getAttributeValue( NonblessedUtils.OBJECTTYPE_ATTRIBUTE )))
                        {
                            return DefaultHtmlHandlebarsViewletFactoryChoice.createSeveral(
                                    VL_NAME,
                                    resourceManager,
                                    ResourceHelper.getInstance( VL_NAME + ".VL", ModuleInit.class.getClassLoader() ),
                                    WebViewletPlacement.ROOT,    ViewletDimensionality.DIM_2D, ViewletDetail.NORMAL,   ViewletFactoryChoice.GOOD_MATCH_QUALITY,
                                    WebViewletPlacement.INLINED, ViewletDimensionality.DIM_2D, ViewletDetail.OVERVIEW, ViewletFactoryChoice.AVERAGE_MATCH_QUALITY );
                        } else {
                            return null;
                        }
                    } );

            Underbars.registerAssets( resourceManager.getAssetMap() );

            resourceManager.checkResources();

        } catch( IOException ex ) {
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }
    }
}
