//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.vl.transactionlog;

import java.util.Date;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.history.MeshBaseState;
import net.ubos.underbars.resources.TemplateMap;
import net.ubos.underbars.vl.AbstractHtmlHandlebarsViewlet;
import net.ubos.util.DateTimeUtil;
import net.ubos.util.ResourceHelper;
import net.ubos.util.cursoriterator.CursorIterator;
import net.ubos.util.cursoriterator.PagingCursorIterator;
import net.ubos.util.cursoriterator.history.HistoryCursorIterator;
import net.ubos.util.logging.Log;
import net.ubos.vl.CannotViewException;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.web.vl.DefaultWebViewedMeshObjects;
import net.ubos.web.vl.PaginatingWebViewlet;
import net.ubos.web.vl.WebMeshObjectsToView;
import net.ubos.web.vl.WebViewedMeshObjects;
import net.ubos.web.vl.WebViewletPlacement;

/**
 * Displays the Transaction Log of a MeshBase
 */
public class TransactionLogViewlet
    extends
        AbstractHtmlHandlebarsViewlet
    implements
        PaginatingWebViewlet<MeshBaseState>
{
    private static final Log log = Log.getLogInstance( TransactionLogViewlet.class );

    /**
     * Factory method.
     *
     * @param name the Viewlet name
     * @param templateMap the named Templates used for the Viewlet
     * @param placement the placement of this Viewlet
     * @param dimensionality the dimensionality of this Viewlet
     * @param detail the level of detail of this Viewlet
     * @return the created Viewlet
     */
    public static TransactionLogViewlet create(
            String                             name,
            TemplateMap                        templateMap,
            WebViewletPlacement                placement,
            ViewletDimensionality              dimensionality,
            ViewletDetail                      detail )
    {
        return create( name, templateMap, DEFAULT_PAGE_LENGTH, placement, dimensionality, detail );
    }

    /**
     * Factory method.
     *
     * @param name the Viewlet name
     * @param templateMap the named Templates used for the Viewlet
     * @param defaultPageLength the default page length
     * @param placement the placement of this Viewlet
     * @param dimensionality the dimensionality of this Viewlet
     * @param detail the level of detail of this Viewlet
     * @return the created Viewlet
     */
    public static TransactionLogViewlet create(
            String                             name,
            TemplateMap                        templateMap,
            int                                defaultPageLength,
            WebViewletPlacement                placement,
            ViewletDimensionality              dimensionality,
            ViewletDetail                      detail )
    {
        DefaultWebViewedMeshObjects viewed = new DefaultWebViewedMeshObjects( placement, dimensionality, detail );

        TransactionLogViewlet ret = new TransactionLogViewlet(
                name,
                templateMap,
                viewed,
                defaultPageLength );

        viewed.setViewlet( ret );
        return ret;
    }

    /**
     * Private constructor, use factory method.
     *
     * @param viewletName the computable name of the Viewlet
     * @param templateMap the named Templates used for the Viewlet
     * @param viewed the WebViewedMeshObjects to use
     * @param defaultPageLength the default page length
     */
    protected TransactionLogViewlet(
            String                             viewletName,
            TemplateMap                        templateMap,
            WebViewedMeshObjects               viewed,
            int                                defaultPageLength )
    {
        super( viewletName, templateMap, viewed );

        theDefaultPageLength = defaultPageLength;
    }

    /**
     * Obtain a CursorIterator over the to-be-paged content.
     *
     * @return the CursorIterator
     */
    protected HistoryCursorIterator<MeshBaseState> getContentIterator()
    {
        if( theContentIterator == null ) {
            MeshBase mb = getSubject().getMeshBase();

            theContentIterator = mb.getHistory().iterator();
        }
        return theContentIterator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HistoryCursorIterator<MeshBaseState> getCurrentPageStartIterator()
        throws
            CannotViewException
    {
        HistoryCursorIterator<MeshBaseState> iter = getContentIterator();

        String pageStart = (String) theViewedMeshObjects.getViewletParameter( PAGE_START_NAME );

        // leave the CursorIterator at the place we got it, and only move it a page start has been specified
        if( pageStart != null ) {
            try {
                Date start = DateTimeUtil.rfc3339ToDate( pageStart );

                iter.moveToJustBeforeTime( start.getTime() );

            } catch( Throwable t ) {
                throw new CannotViewException.InvalidParameter( this, PAGE_START_NAME, pageStart, null );
            }
        }
        return iter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CursorIterator<MeshBaseState> getWithinPageIterator()
        throws
            CannotViewException
    {
        HistoryCursorIterator<MeshBaseState> pageStart = getCurrentPageStartIterator();
        HistoryCursorIterator<MeshBaseState> delegate  = pageStart.createCopy(); // don't move page start around

        return PagingCursorIterator.create( getPageSize(), delegate );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageSize()
    {
        int ret;

        String pageLengthString = (String) theViewedMeshObjects.getViewletParameter( PAGE_LENGTH_NAME );
        if( pageLengthString != null ) {
            ret = Integer.getInteger( pageLengthString );
            if( ret < 1 ) {
                ret = theDefaultPageLength;
            }
        } else {
                ret = theDefaultPageLength;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDestinationUrlForStart(
            MeshBaseState startLocation )
    {
        long   when   = startLocation.getTimeUpdated();
        String marker = DateTimeUtil.dateToRfc3339( new Date( when ));

        WebMeshObjectsToView toView = getViewedMeshObjects().getMeshObjectsToView();

        toView = toView.withViewletParameter( PAGE_START_NAME, marker );

        return toView.asUrlString();
    }

    /**
     * Default page length.
     */
    protected final int theDefaultPageLength;

    /**
     * The iterator over the content, allocated as needed.
     */
    protected HistoryCursorIterator<MeshBaseState> theContentIterator;

    /**
     * Default page size.
     */
    public static final int DEFAULT_PAGE_LENGTH = ResourceHelper.getInstance( TransactionLogViewlet.class ).getResourceIntegerOrDefault(
            "DefaultPageLength",
            20 );

    /**
     * Name of the Viewlet parameter indicating the MeshObject with which the current page starts.
     */
    public static final String PAGE_START_NAME = "page-start";

    /**
     * Name of the Viewlet parameter indicating the page length.
     */
    public static final String PAGE_LENGTH_NAME = "page-length";
}
