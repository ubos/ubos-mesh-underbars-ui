//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.underbars.skin.bare;

import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Diet4j module activation.
     */
public class ModuleInit
{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "ubos-mesh-underbars-skin-bare";

    /**
     * Name of the subprojects.
     */
    public static final String SUBPROJ_NAME = "net.ubos.underbars.skin.bare";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

        Underbars.registerAssets( resourceManager.getAssetMap() );
        Underbars.registerSkinsFromManager( resourceManager.getSkinManager() );

        Underbars.registerFragments( resourceManager.getFragmentTemplateMap() );

        if( Underbars.getDefaultSkinName() == null ) {
            Underbars.setDefaultSkinName( SUBPROJ_NAME );
        }
    }
}
