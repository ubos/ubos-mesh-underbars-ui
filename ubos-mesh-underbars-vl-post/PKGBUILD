pkgname=$(basename $(pwd))
pkgver=$(cat ../PKGVER)
pkgrel=1
pkgdesc='UBOS Mesh Post Viewlet using Handlebars and Undertow'
developer="https://indiecomputing.com/"
url="https://ubos.net/"
maintainer=${developer}
arch=('any')
license=('AGPL3')
options=('!strip')
_groupId='net.ubos.underbars'
_jv=$(cat ../JV)
_m2repo=${GRADLE_M2_HOME:-${HOME}/.m2/repository}

depends=(
    'ubos-mesh-model-facebook'
    'ubos-mesh-model-library'
    'ubos-mesh-underbars'
)
makedepends=(
    'gradle'
)

build() {
    cd ${startdir}

    if [[ "${GRADLE_M2_HOME}" != '' ]]; then
        export M2_HOME=${GRADLE_M2_HOME}
    fi
    JAVA_HOME=/usr/lib/jvm/java-${_jv}-openjdk gradle jar
}

package() {
    # Manifest
    install -D -m0644 ${startdir}/ubos-manifest.json ${pkgdir}/ubos/lib/ubos/manifests/${pkgname}.json

    # Icons
    # install -D -m0644 ${startdir}/appicons/{72x72,144x144}.png -t ${pkgdir}/ubos/http/_appicons/${pkgname}/

    # Assets and viewlets
    mkdir -p ${pkgdir}/ubos/share/${pkgname}
    cp -a ${startdir}/{assets,viewlets} ${pkgdir}/ubos/share/${pkgname}/

    # Code
    installJar 'net.ubos.underbars.vl.post'
    installJar 'ubos-mesh-underbars-vl-post'
}

installJar() {
    local name=$1
    install -m644 -D ${_m2repo}/${_groupId//.//}/${name}/${pkgver}/${name}-${pkgver}.{jar,pom} -t ${pkgdir}/ubos/lib/java/${_groupId//.//}/${name}/${pkgver}/
}
